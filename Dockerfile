FROM python:3.11.4-alpine


WORKDIR /usr/src/app

RUN python -m pip install --upgrade pip

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

# RUN django-admin startproject mysite

COPY . ./
# RUN python manage.py collectstatic --no-input
# EXPOSE 8000
CMD ["gunicorn", "--config", "gunicorn-cfg.py", "core.wsgi"]

CMD ["uvicorn", "--host", "0.0.0.0", "--reload", "credimax.asgi:application"]

# Start the Django app
# CMD ["gunicorn", "--config", "gunicorn-cfg.py", "core.wsgi"]