from django.urls import path

# from admin_datta import views

from . import views

app_name = "users"
urlpatterns = [
    path("", views.lists, name="lists"),
    # path("", views.lists, name="lists"),
    path("create", views.create, name="create"),
    # path("store", views.store, name="store"),
]
