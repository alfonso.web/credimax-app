import factory
from factory.django import DjangoModelFactory
from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

# from .models import User

class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    password = make_password('secret')
    last_login = timezone.now()
    is_superuser = False
    username = factory.Faker("slug")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    is_staff = False
    is_active = True
    date_joined = timezone.now()

user = UserFactory()
user.password
user.last_login
user.is_superuser
user.username
user.first_name
user.last_name
user.email
user.is_staff
user.is_active
user.date_joined