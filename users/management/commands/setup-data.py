import json, random
from django.db import transaction
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from users.factories import (UserFactory)
from django.contrib.auth.hashers import make_password


class Command(BaseCommand):

    @transaction.atomic
    def handle(self, *args, **kwargs):

        self.stdout.write('Deleting old data...')
        model = [User]
        for m in model:
            m.objects.all().delete()

        self.stdout.write("Creating new data...")
        # Create all the users

        for _ in range(20):
            UserFactory()
