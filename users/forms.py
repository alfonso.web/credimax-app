from django.forms import ModelForm, TextInput, EmailInput
from django.contrib.auth.models import User
from django import forms
from django.core import validators

class UserForm(forms.Form):
    # class Meta:
    #     model = User
    #     fields = ['username']
        username = forms.CharField(widget = forms.TextInput(
            attrs = {'placeholder': 'Username'}),
            label = 'Username',
        )
        first_name = forms.CharField(widget = forms.TextInput(
            attrs = {'placeholder': 'First name'}),
            label = 'First name',
        )
        last_name = forms.CharField(widget = forms.TextInput(
            attrs = {'placeholder': 'Last name'}),
            label = 'Last name',
        )
        email = forms.EmailField(widget = forms.TextInput(
            attrs = {'placeholder': 'Email'}),
            label = 'Email',
            validators=[validators.EmailValidator(message="Invalid Email")]
        )