from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from .forms import UserForm


# Create your views here.
def lists(request):
    users = User.objects.order_by('-date_joined')
    context = {
        "users": users,
    }
    return render(request, "pages/users/lists.html", context)


def create(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if(form.is_valid()):
            pass
    else:
        form = UserForm()
    #     user = User.objects.create(
    #         username = request.POST["username"],
    #         first_name = request.POST["first_name"],
    #         last_name = request.POST["last_name"],
    #         email = request.POST["email"],
    #         password = make_password("secret"),
    #     )
    return render(request, "pages/users/create.html", {'form': form})
